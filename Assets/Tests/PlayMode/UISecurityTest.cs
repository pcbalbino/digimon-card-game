﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class UISecurityTest
    {
        private GameObject playmat;

        [UnityTest]
        public IEnumerator UISecurity_WhenInitialised_ShouldHaveP1andP2SecurityGameObjectsReference()
        {
            playmat = Resources.Load<GameObject>("Prefabs/Playmat");
            playmat = GameObject.Instantiate(playmat);

            yield return null;

            GameObject secGameObject = new GameObject("UISecurityGameObject");
            UISecurity uISecurity = secGameObject.AddComponent<UISecurity>();

            yield return null;

            Assert.IsNotNull(uISecurity.p1Security);
            Assert.IsNotNull(uISecurity.p2Security);
        }

        [UnityTest]
        public IEnumerator UISecurity_WhenAddingCardToSecurityEventWithPlayer1Trigerred_ShouldAddCardToSecurityStack(){
            playmat = Resources.Load<GameObject>("Prefabs/Playmat");
            playmat = GameObject.Instantiate(playmat);

            yield return null;

            GameObject secGameObject = new GameObject("UISecurityGameObject");
            UISecurity uISecurity = secGameObject.AddComponent<UISecurity>();

            yield return null;

            GameObject mockCard = new GameObject();
            bool isPlayer1 = true;

            EventBroker.CallAddedCardToSecurity(mockCard, isPlayer1);

            yield return null;

            Assert.AreEqual(1, uISecurity.p1CurrentSecurityCount);
            Assert.AreEqual(mockCard, uISecurity.p1Security.transform.GetChild(uISecurity.p1CurrentSecurityCount-1).GetChild(0).gameObject);
        }

                [UnityTest]
        public IEnumerator UISecurity_WhenAddingCardToSecurityEventWithPlayer2Trigerred_ShouldAddCardToSecurityStack(){
            playmat = Resources.Load<GameObject>("Prefabs/Playmat");
            playmat = GameObject.Instantiate(playmat);

            yield return null;

            GameObject secGameObject = new GameObject("UISecurityGameObject");
            UISecurity uISecurity = secGameObject.AddComponent<UISecurity>();

            yield return null;

            GameObject mockCard = new GameObject();
            bool isPlayer1 = false;

            EventBroker.CallAddedCardToSecurity(mockCard, isPlayer1);

            yield return null;

            Assert.AreEqual(1, uISecurity.p2CurrentSecurityCount);
            Assert.AreEqual(mockCard, uISecurity.p2Security.transform.GetChild(uISecurity.p2CurrentSecurityCount-1).GetChild(0).gameObject);
        }
    }
}
