﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Mirror;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Moq;

namespace Tests
{
    public class PlayerTest
    {
        public class DeckTest{
            private GameObject testDeckPrefab;

            [SetUp]
            public void SetUp(){
                testDeckPrefab = Resources.Load<GameObject>("Prefabs/Decks/Sample_Deck");
            }

            [UnityTest]
            public IEnumerator Deck_Initialised_IsNotNull(){
                DeckV2 player = new DeckV2(testDeckPrefab, testDeckPrefab);
                
                yield return null;
                
                Assert.IsNotNull(player.cardsList);
            }

            [UnityTest]
            public IEnumerator Deck_IsLoaded_WithValidPrefab_Has50CardsAndContainBasicCardScripts(){
                DeckV2 player = new DeckV2(testDeckPrefab, testDeckPrefab);

                player.LoadDeck();
                
                yield return null;

                Assert.AreEqual(50, player.cardsList.Count);

                foreach(GameObject card in player.cardsList){
                    var basicCard = card.GetComponentInChildren<BasicCard>();

                    Assert.IsNotNull(basicCard);
                }
            }

            [UnityTest]
            public IEnumerator Deck_IsLoaded_WithInvalidPrefab_ThrowsAnException(){
                DeckV2 player = new DeckV2(new GameObject(), new GameObject());

                yield return null;

                //Assert.Throws<ArgumentException>(() => player.LoadDeck());
            }

            [UnityTest]
            public IEnumerator Deck_IsShuffled_ShouldHaveDifferentCardOrder(){
                DeckV2 player = new DeckV2(testDeckPrefab, testDeckPrefab);
                player.LoadDeck();

                yield return null;
                
                List<GameObject> originalDeck = player.cardsList.ToList<GameObject>();
                player.ShuffleDeck();

                Assert.IsFalse(player.cardsList.SequenceEqual(originalDeck));
                Assert.AreEqual(originalDeck.Count, player.cardsList.Count);
            }

            [UnityTest]
            public IEnumerator DeckWith50Cards_OnDraw_ShouldReturnCardObject(){
                DeckV2 player = new DeckV2(testDeckPrefab, testDeckPrefab);
                player.LoadDeck();

                yield return null;

                GameObject drawnCard = player.DrawCard();

                Assert.IsNotNull(drawnCard);
                Assert.IsNotNull(drawnCard.GetComponentInChildren<BasicCard>());
            }

            [UnityTest]
            public IEnumerator DeckWith0Cards_OnDraw_ShouldThrowException(){
                DeckV2 deck = new DeckV2(testDeckPrefab, testDeckPrefab);
                deck.LoadDeck();

                yield return null;

                while(deck.cardsList.Count != 0){
                    deck.DrawCard();
                }

                Assert.Throws<ArgumentOutOfRangeException>(() => deck.DrawCard());
            }

            [UnityTest]
            public IEnumerator Deck_OnDraw_ShouldRaiseDeckCountCardDrawnEvent(){
                DeckV2 deck = new DeckV2(testDeckPrefab, testDeckPrefab);
                deck.LoadDeck();
                int count = -1;

                EventBroker.CardDrawn += (remainingCards) =>{
                    count = remainingCards;
                };

                deck.DrawCard();

                yield return null;

                Assert.AreEqual(deck.cardsList.Count, count);
            }            
        }

        public class SecurityTests{
            private GameObject emptyGameObject; 
            private GameObject playerPrefab;
            private Player player;

            [SetUp]
            public void SetUp(){
                emptyGameObject = Resources.Load<GameObject>("Prefabs/Decks/Sample_Deck");
                player = new Player();
            }

            [UnityTest]
            public IEnumerator Player_OnInitialise_LoadSecurity(){
                player.deckPrefab = emptyGameObject;

                yield return null;

                Assert.IsNotNull(player.security);
            }
            
            [UnityTest]
            public IEnumerator Player_OnStartGame_ShouldHave5SecuritiesAnd5CardsInHand(){
                var deckMock = new Mock<DeckV2>();
                player.deck = deckMock.Object;
                deckMock.Setup(deck => deck.DrawCard()).Returns(emptyGameObject);
                
                player.StartGame();

                yield return null;

                Assert.AreEqual(5, player.hand.Count);
                Assert.AreEqual(5, player.security.Count);
            }

            [UnityTest]
            public IEnumerator PlayerWith5Securities_OnAddSecurityCard_ShouldHave6Securities(){
                var deckMock = new Mock<DeckV2>();
                var cardMock = new GameObject();
                cardMock.AddComponent<BasicCard>();
                player.deck = deckMock.Object;
                deckMock.Setup(deck => deck.DrawCard()).Returns(cardMock);
                player.StartGame();

                yield return null;
                player.AddSecurity(cardMock);

                Assert.AreEqual(6, player.security.Count);
            }

            [UnityTest]
            public IEnumerator PlayerWith5Securities_OnAddNonCardObject_ShouldThrowException(){
                yield return null;
                Assert.Throws<ArgumentException>(() => player.AddSecurity(new GameObject()));
            }

            [UnityTest]
            public IEnumerator PlayerWith5Securities_OnCheckSecurityCard_ShouldHave4Securities(){
                var deckMock = new Mock<DeckV2>();
                var cardMock = new GameObject();
                cardMock.AddComponent<BasicCard>();
                player.deck = deckMock.Object;
                deckMock.Setup(deck => deck.DrawCard()).Returns(cardMock);
                player.StartGame();

                yield return null;

                player.CheckSecurity(cardMock);

                Assert.AreEqual(4, player.security.Count);
            }

            [UnityTest]
            public IEnumerator PlayerWith0Securities_OnCheckSecurityCard_ShouldThrowException(){
                var deckMock = new Mock<DeckV2>();
                var cardMock = new GameObject();
                cardMock.AddComponent<BasicCard>();
                player.deck = deckMock.Object;
                deckMock.Setup(deck => deck.DrawCard()).Returns(cardMock);
                player.StartGame();

                yield return null;

                player.security.Clear();
                Assert.Throws<ArgumentOutOfRangeException>(()=> player.CheckSecurity(cardMock));
            }

            [UnityTest]
            public IEnumerator PlayerWith5Securities_OnCheckSecurityCardWithCardNotPresentOnThePile_ShouldThrowAnException()
            {
                var deckMock = new Mock<DeckV2>();
                var cardMock = new GameObject();
                var invalidCard = new GameObject();
                cardMock.AddComponent<BasicCard>();
                deckMock.Setup(deck => deck.DrawCard()).Returns(cardMock);
                player.deck = deckMock.Object;
                player.StartGame();

                yield return null;
                
                Assert.Throws<ArgumentException>(()=> player.CheckSecurity(invalidCard));
            }
        }
    
        public class BattleCardsTest{
            private GameObject cardMock; 
            private GameObject playerPrefab;
            private Player player;

            [SetUp]
            public void SetUp(){
                player = new Player();

                // Creates mocked card
                cardMock = new GameObject();
                cardMock.AddComponent<BasicCard>();

                // Setup Mocks for Deck with Mock Card
                var deckMock = new Mock<DeckV2>();
                deckMock.Setup(deck => deck.DrawCard()).Returns(cardMock);
                player.deck = deckMock.Object;

                player.StartGame();
            }

            [UnityTest]
            public IEnumerator PlayerOnInitialise_StartingGame_ShouldHaveNonNullBattleCards(){
                Assert.IsNotNull(player.battleCards);
                yield return null;
            }

            [UnityTest]
            public IEnumerator PlayerWith0BattleCards_OnAddBattleCardWithValidCard_ShouldAddThatCardToBattleList(){
                GameObject newCard = new GameObject();
                newCard.AddComponent<BasicCard>();

                player.AddCardToBattleArea(newCard);
                
                Assert.IsTrue(player.battleCards.Contains(newCard));
                Assert.AreEqual(1, player.battleCards.Count);
                yield return null;
            }

            [UnityTest]
            public IEnumerator PlayerWith0BattleCards_OnAddBattleCardWithInvalidCard_ShouldThrowException(){
                GameObject newCard = new GameObject();
                
                Assert.Throws<ArgumentException>(() => player.AddCardToBattleArea(newCard));
                yield return null;                
            }

            [UnityTest]
            public IEnumerator PlayerWith1Battlecard_OnAddBattleCardWithNull_ShouldThrowException(){
                GameObject newCard = new GameObject();
                newCard.AddComponent<BasicCard>();

                player.AddCardToBattleArea(newCard);
                Assert.Throws<ArgumentException>(() => player.AddCardToBattleArea(null));
                yield return null;                 
            }

            [UnityTest]
            public IEnumerator PlayerWith2BattleCards_OnRemoveValidBattleCard_ShouldHave1BattleCardLeft(){
                GameObject newCard = new GameObject();;
                for(int i=0; i<2; i++){
                    newCard = new GameObject();
                    newCard.AddComponent<BasicCard>();
                    player.AddCardToBattleArea(newCard);
                }

                player.RemoveCardFromBattleArea(newCard);

                Assert.AreEqual(1, player.battleCards.Count);
                Assert.IsFalse(player.battleCards.Contains(newCard));
                yield return null;
            }

            [UnityTest]
            public IEnumerator PlayerWith0BattleCards_OnRemoveCardWithAnyCard_ThrowsException(){
                GameObject card = new GameObject();

                Assert.Throws<ArgumentOutOfRangeException>(()=> player.RemoveCardFromBattleArea(card));
                yield return null;
            }

            [UnityTest]
            public IEnumerator PlayerWith2BattleCards_OnRemoveCardWithInvalidCard_ThrowsException(){
                GameObject newCard = new GameObject();;
                for(int i=0; i<2; i++){
                    newCard = new GameObject();
                    newCard.AddComponent<BasicCard>();
                    player.AddCardToBattleArea(newCard);
                }
            
                GameObject invalidCard = new GameObject();

                Assert.Throws<ArgumentException>(()=> player.RemoveCardFromBattleArea(invalidCard));
                Assert.AreEqual(2, player.battleCards.Count);
                yield return null;
            }

        }
    
        public class DrawCardAndHandTests{
            private GameObject cardMock; 
            private GameObject playerPrefab;
            private Player player;

            [SetUp]
            public void SetUp(){
                player = new Player();

                // Creates mocked card
                cardMock = new GameObject();
                cardMock.AddComponent<BasicCard>();

                // Setup Mocks for Deck with Mock Card
                var deckMock = new Mock<DeckV2>();
                deckMock.Setup(deck => deck.DrawCard()).Returns(cardMock);
                player.deck = deckMock.Object;

                player.StartGame();
            }

            [UnityTest]
            public IEnumerator PlayerWithFullDeck_WhenDrawingCard_ShouldReturnValidCard(){
                GameObject card = player.DrawCard();

                Assert.IsNotNull(card);
                Assert.IsNotNull(card.GetComponentInChildren<BasicCard>());
                yield return null;
            }

            [UnityTest]
            public IEnumerator PlayerWithEmptyHand_WhenAddingCardToHand_ShouldHave1Card(){
                player.hand.Clear();
                player.AddCardToHand(cardMock);

                Assert.AreEqual(1, player.hand.Count);
                Assert.IsTrue(player.hand.Contains(cardMock));
                yield return null;
            }

            [UnityTest]
            public IEnumerator PlayerWith5CardsOnHand_WhenAddingInvalidCardToHand_ThrowsException(){
                GameObject invalidCard = new GameObject();
                Assert.Throws<ArgumentException>(() => player.AddCardToHand(invalidCard));
                yield return null;
            }

            [UnityTest]
            public IEnumerator PlayerWith5CardsOnHand_WhenRemovingCardFromHand_ShouldHave4CardsLeftAndNotHaveRemovedCard(){
                GameObject newCard = new GameObject();
                newCard.AddComponent<BasicCard>();
                player.hand.Add(newCard);
                player.RemoveCardFromHand(newCard);

                Assert.IsFalse(player.hand.Contains(newCard));
                Assert.AreEqual(5, player.hand.Count);
                yield return null;
            }

            [UnityTest]
            public IEnumerator PlayerWith5CardsOnHand_WhenRemovingInvalidCardFromHand_ThrowsException(){
                GameObject invalidCard = new GameObject();
                
                Assert.Throws<ArgumentException>(() => player.RemoveCardFromHand(invalidCard));
                Assert.AreEqual(5, player.hand.Count);
                yield return null;
            }

            [UnityTest]
            public IEnumerator PlayerWith0CardsOnHand_WhenRemovingACard_ThrowsException(){
                player.hand.Clear();

                Assert.Throws<ArgumentOutOfRangeException>(() => player.RemoveCardFromHand(cardMock));
                yield return null;
            }
        }
    
        public class HatchingAndRaisingTests{
            private GameObject cardMock; 
            private GameObject playerPrefab;
            private Player player;

            [SetUp]
            public void SetUp(){
                player = new Player();

                // Creates mocked card
                cardMock = new GameObject();
                cardMock.AddComponent<BasicCard>();

                // Setup Mocks for Deck with Mock Card
                var deckMock = new Mock<DeckV2>();
                deckMock.Setup(deck => deck.DrawCard()).Returns(cardMock);
                player.deck = deckMock.Object;

                // Setup Mocks for Digitama Deck with Mock Card
                var digitamaDeckMock = new Mock<DeckV2>();
                digitamaDeckMock.Setup(deck => deck.DrawCard()).Returns(cardMock);
                player.digitamaDeck = digitamaDeckMock.Object;

                player.StartGame();
            }

            [UnityTest]
            public IEnumerator PlayerWithFullDigitamDeck_WhenHatchingEgg_ShouldSetRaisingCardAndReturnIt(){
                GameObject card = player.HatchEgg();

                Assert.IsNotNull(card);
                Assert.IsNotNull(player.raisingCard);
                yield return null;
            }

            [UnityTest]
            public IEnumerator PlayerWithFullDigitamaDeck_WhenHatchingEggWithRaisingDigimonPresent_ShouldThrowError(){
                GameObject exisintRaisingCard= player.HatchEgg();
                Assert.Throws<InvalidOperationException>(()=>player.HatchEgg());
                Assert.AreEqual(exisintRaisingCard, player.raisingCard);
                yield return null;
            }

            [UnityTest]
            public IEnumerator PlayerWithRaisingCard_WhenMovingRaisingCardToBattleArea_ShouldHaveNullRaisingCardAndOneExtraBattleCard(){
                player.HatchEgg();

                player.MoveRaisingCardToBattleArea();
                
                Assert.IsNull(player.raisingCard);
                Assert.AreEqual(1, player.battleCards.Count);
                yield return null;
            }

            [UnityTest]
            public IEnumerator PlayerWithoutRaisingCard_WhenMovingRaisingCardToBattleArea_ThrowsException(){
                int previousBattleCardsCount = player.battleCards.Count;
                Assert.Throws<InvalidOperationException>(() => player.MoveRaisingCardToBattleArea());
                Assert.AreEqual(previousBattleCardsCount, player.battleCards.Count);
                yield return null;
            }
        }

        public class DiscardPileTest{
            [UnityTest]
            public IEnumerator PlayerWithEmptyDiscardPile_WhenMovingCardToDiscardPile_ShouldAddItToThPile(){
                Player player = new Player();
                GameObject mockCard = new GameObject();
                mockCard.AddComponent<BasicCard>();

                //player.AddDiscardPile(mockCard);
                
                Assert.AreEqual(1, player.discardPile.Count);
                Assert.IsTrue(player.discardPile.Contains(mockCard));
                yield return null;
            }

            [UnityTest]
            public IEnumerator PlayerWithEmptyDiscardPile_WhenMovingInvalidCardToDiscardPile_ThrowsException(){
                Player player = new Player();
                GameObject invalidCard = new GameObject();

                //Assert.Throws<ArgumentException>(() => player.AddDiscardPile(invalidCard));
                yield return null;
            }

            [UnityTest]
            public IEnumerator Player_WhenMovingNullToDiscardPile_ThrowsException(){
                Player player = new Player();

                //Assert.Throws<ArgumentException>(() => player.AddDiscardPile(null));
                yield return null;
            }
        }

        public class TamerAreaTest{
            [UnityTest]
            public IEnumerator PlayerWithNoTamers_WhenAddingValidTamerCard_ShouldHave1Tamer(){
                Player player = new Player();
                GameObject card = new GameObject();
                card.AddComponent<BasicCard>().type = BasicCard.TYPE.TAMER;

                player.AddTamerToBattleArea(card);

                Assert.AreEqual(1, player.tamers.Count);
                Assert.IsTrue(player.tamers.Contains(card));
                yield return null;
            }

            [UnityTest]
            public IEnumerator PlayerWithNoTamers_WhenAddingNonTamerCard_ThrowsException(){
                Player player = new Player();
                GameObject nonTamerCard = new GameObject();
                nonTamerCard.AddComponent<BasicCard>().type = BasicCard.TYPE.DIGIMON;

                Assert.Throws<ArgumentException>(() => player.AddTamerToBattleArea(nonTamerCard));
                Assert.AreEqual(0, player.tamers.Count);
                yield return null;
            }

            [UnityTest]
            public IEnumerator Player_WhenAddingInvalidCard_ThrowsException(){
                Player player = new Player();
                GameObject invalidCard = new GameObject();

                Assert.Throws<ArgumentException>(() => player.AddTamerToBattleArea(invalidCard));
                Assert.AreEqual(0, player.tamers.Count);
                yield return null;
            }
        }
    }
}
