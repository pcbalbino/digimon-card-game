﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;

namespace Tests
{
    public class UIManagerTest
    {
        public class Player_1_Area_Tests
        {
            public class LoadUp
            {
                private GameObject playmat;
                private GameObject uIManagerObject;
                private UIManager uIManager;

                [SetUp]
                public void SetUp()
                {
                    playmat = Resources.Load<GameObject>("Prefabs/Playmat");
                    GameObject.Instantiate(playmat);
                }

                [UnityTest]
                public IEnumerator UIManager_Initialised_ShouldHavePlayer1AreaReference()
                {
                    uIManagerObject = new GameObject("UIManager");
                    UIManager uIManager = uIManagerObject.AddComponent<UIManager>();
                    GameObject.Instantiate(uIManagerObject);

                    yield return null;

                    Assert.IsNotNull(uIManager.p1Area);
                }

                [UnityTest]
                public IEnumerator UIManager_Initialised_ShouldHavePlayer1HandReference()
                {
                    uIManagerObject = new GameObject("UIManager");
                    UIManager uIManager = uIManagerObject.AddComponent<UIManager>();
                    GameObject.Instantiate(uIManagerObject);

                    yield return null;

                    Assert.IsNotNull(uIManager.p1Hand);
                }

                [UnityTest]
                public IEnumerator UIManager_Initialised_ShouldHavePlayer1SecurityReference(){
                    uIManagerObject = new GameObject("UIManager");
                    UIManager uIManager = uIManagerObject.AddComponent<UIManager>();
                    GameObject.Instantiate(uIManagerObject);

                    yield return null;

                    //Assert.IsNotNull(uIManager.p1Security);
                }
                
            }


        }


    }
}
