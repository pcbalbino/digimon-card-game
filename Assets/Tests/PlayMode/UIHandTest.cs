﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using Moq;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class UIHandTest
    {
        private GameObject playmat;
        private GameObject mockCard;
        private UIHand uIHand;
        private GameObject uIHandGameObject;

        [SetUp]
        public void SetUp()
        {
            playmat = Resources.Load<GameObject>("Prefabs/Playmat");
            playmat = GameObject.Instantiate(playmat);
            mockCard = new GameObject();
        }

        [UnityTest]
        public IEnumerator HandUI_WhenInitialised_ShouldHaveP1andP2HandGameObjectsReference()
        {
            uIHandGameObject = new GameObject("UIHand");
            uIHand = uIHandGameObject.AddComponent<UIHand>();

            yield return null;

            Assert.IsNotNull(uIHand.p1Hand);
            Assert.IsNotNull(uIHand.p2Hand);
        }

        [UnityTest]
        public IEnumerator HandUI_WhenAddedCardToHandEventWithPlayer1Triggered_ShouldAddCardToPlayer1Hand()
        {
            uIHandGameObject = new GameObject("UIHand");
            uIHand = uIHandGameObject.AddComponent<UIHand>();

            yield return null;
                            
            EventBroker.CallAddedCardToHand(mockCard, true);

            // Wait for Animation to Complete
            yield return new WaitForSeconds(0.5f);

            Assert.AreEqual(1, uIHand.p1Hand.transform.childCount);
            Assert.AreEqual(mockCard, uIHand.p1Hand.transform.GetChild(0).gameObject);
        }

        [UnityTest]
        public IEnumerator HandUI_WhenAddedCardToHandEventWithPlayer2Triggered_ShouldAddCardToPlayer2Hand(){
            uIHandGameObject = new GameObject("UIHand");
            uIHand = uIHandGameObject.AddComponent<UIHand>();

            yield return null;
                            
            EventBroker.CallAddedCardToHand(mockCard, false);

            // Wait for Animation to Complete
            yield return new WaitForSeconds(0.5f);

            Assert.AreEqual(1, uIHand.p2Hand.transform.childCount);
            Assert.AreEqual(mockCard, uIHand.p2Hand.transform.GetChild(0).gameObject);
        }

    }
}
