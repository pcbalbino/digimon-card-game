﻿using System.Collections;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.UI;

namespace Tests
{
    public class UIRaisingTest
    {
        private GameObject playmat;
        private GameObject mockCard;
        private GameObject cardPrefab;
        private RaisingAreaUI uIRaising;
        private GameObject uIRaisingGameObject;

        [SetUp]
        public void SetUp()
        {
            playmat = Resources.Load<GameObject>("Prefabs/Playmat");
            playmat = GameObject.Instantiate(playmat);
            cardPrefab = Resources.Load<GameObject>("Prefabs/Cards/BasicCard");
            mockCard = GameObject.Instantiate(cardPrefab);
        }

        [UnityTest]
        public IEnumerator RaisingUI_WhenInitialised_ShouldHaveP1andP2DeckAndHolderObjectsReference()
        {
            uIRaisingGameObject = new GameObject("UIRaisingArea");
            uIRaising = uIRaisingGameObject.AddComponent<RaisingAreaUI>();

            yield return null;

            Assert.IsNotNull(uIRaising.p1BabyHolder);
            Assert.IsNotNull(uIRaising.p1RaisingDeck);
            Assert.IsNotNull(uIRaising.p2BabyHolder);
            Assert.IsNotNull(uIRaising.p2RaisingDeck);
        }

        [UnityTest]
        public IEnumerator RaisingUI_WhenPlacingP1DigimonInRaisingArea_ShouldSetCardInP1BabyHolder()
        {
            uIRaisingGameObject = new GameObject("UIRaisingArea");
            uIRaising = uIRaisingGameObject.AddComponent<RaisingAreaUI>();

            yield return null;

            uIRaising.PlaceDigimonInRaisingArea(mockCard, true);

            yield return null;

            Assert.AreEqual(1, uIRaising.p1BabyHolder.transform.childCount);
            Assert.AreEqual(mockCard, uIRaising.p1BabyHolder.transform.GetChild(0).gameObject);
            Assert.IsFalse(uIRaising.p1RaisingDeck.GetComponent<Button>().interactable);
        }

        [UnityTest]
        public IEnumerator RaisingUI_WhenPlacingP2DigimonInRaisingArea_ShouldSetCardInP2BabyHolder()
        {
            uIRaisingGameObject = new GameObject("UIRaisingArea");
            uIRaising = uIRaisingGameObject.AddComponent<RaisingAreaUI>();

            yield return null;

            uIRaising.PlaceDigimonInRaisingArea(mockCard, false);

            yield return null;

            Assert.AreEqual(1, uIRaising.p2BabyHolder.transform.childCount);
            Assert.AreEqual(mockCard, uIRaising.p2BabyHolder.transform.GetChild(0).gameObject);
        }

        [UnityTest]
        public IEnumerator RaisingUI_WhenPlacingP1DigimonInRaisingAreaWithADigimonThereAlready_ShouldIgnoreNewCard()
        {
            uIRaisingGameObject = new GameObject("UIRaisingArea");
            uIRaising = uIRaisingGameObject.AddComponent<RaisingAreaUI>();

            yield return null;

            uIRaising.PlaceDigimonInRaisingArea(mockCard, true);

            yield return null;

            uIRaising.PlaceDigimonInRaisingArea(GameObject.Instantiate(cardPrefab), true);

            yield return null;

            Assert.AreEqual(1, uIRaising.p1BabyHolder.transform.childCount);
            Assert.AreEqual(mockCard, uIRaising.p1BabyHolder.transform.GetChild(0).gameObject);
        }

        [UnityTest]
        public IEnumerator RaisingUI_WhenPlacingP2DigimonInRaisingAreaWithADigimonThereAlready_ShouldIgnoreNewCard()
        {
            uIRaisingGameObject = new GameObject("UIRaisingArea");
            uIRaising = uIRaisingGameObject.AddComponent<RaisingAreaUI>();

            yield return null;

            uIRaising.PlaceDigimonInRaisingArea(mockCard, false);

            yield return null;

            uIRaising.PlaceDigimonInRaisingArea(GameObject.Instantiate(cardPrefab), false);

            yield return null;

            Assert.AreEqual(1, uIRaising.p2BabyHolder.transform.childCount);
            Assert.AreEqual(mockCard, uIRaising.p2BabyHolder.transform.GetChild(0).gameObject);
        }

        [UnityTest]
        public IEnumerator RaisingUI_WhenP1Clicks_ThenCallEventToHatchEggs()
        {
            uIRaisingGameObject = new GameObject("UIRaisingArea");
            uIRaising = uIRaisingGameObject.AddComponent<RaisingAreaUI>();

            bool eventCalled = false;
            EventBroker.PlayerHatchesEgg += () =>
            {
                eventCalled = true;
            };

            yield return null;

            uIRaising.HatchEgg();
            Assert.IsTrue(eventCalled);
        }

        [TearDown]
        public void TearDown()
        {
            GameObject.Destroy(playmat);
            GameObject.Destroy(mockCard);
            GameObject.Destroy(uIRaisingGameObject);
        }
    }
}
