﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using System.Linq;
using NUnit.Framework;

public abstract class MonoBehaviourBaseTest{
    // Start is called before the first frame update
    protected GameObject subject;

    [SetUp]
    public virtual void SetUp(){
        subject = new GameObject("Subject");
    }

    [TearDown]
    public virtual void TearDown(){
        Object.FindObjectsOfType<GameObject>().ToList()
        .ForEach(go => Object.DestroyImmediate(go));
    }

    public static BindingFlags _privateFlags = BindingFlags.Instance | BindingFlags.NonPublic;

    public static T GetPrivateReflection<O, T>(O obj, string fieldName){
        return (T)typeof(O).GetField(fieldName, _privateFlags)?.GetValue(obj);
    }
}
