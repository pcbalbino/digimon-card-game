﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class AttackHandlerTest
    {
        [UnityTest]
        public IEnumerator WhenStrongerCardAttackWeaker_ShouldReturnBattleWin()
        {
            BasicCard attacker  = new BasicCard();
            attacker.DP = 4000;

            BasicCard target = new BasicCard();
            target.DP = 2000;

            //System.Enum result = AttackHandler.ResolveAttack(attacker, target);

            //Assert.IsTrue(Enums.BATTLE_OUTCOMES.WIN.Equals(result));
            yield return null;
        }

        [UnityTest]
        public IEnumerator WhenWeakerCardAttackStronger_ShouldReturnBattleLose(){
            BasicCard attacker  = new BasicCard();
            attacker.DP = 2000;

            BasicCard target = new BasicCard();
            target.DP = 4000;

            //System.Enum result = AttackHandler.ResolveAttack(attacker, target);
        
           // Assert.AreEqual(Enums.BATTLE_OUTCOMES.LOSE, result);
            yield return null;
        }

        [UnityTest]
        public IEnumerator WhenAttackerCardHasSameDPAsTarget_ShouldReturnBattleTie(){
            BasicCard attacker  = new BasicCard();
            attacker.DP = 2000;

            BasicCard target = new BasicCard();
            target.DP = 2000;

            //System.Enum result = AttackHandler.ResolveAttack(attacker, target);
        
            //Assert.AreEqual(Enums.BATTLE_OUTCOMES.TIE, result);
            yield return null;
        }
    }
}
