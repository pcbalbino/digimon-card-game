﻿using System;
using System.Collections;
using UnityEngine;

public abstract class State
{
    protected GameManager GameSystem;
    protected PlayerManager Player;

    public State(GameManager gameSystem, PlayerManager player)
    {
        GameSystem = gameSystem;
        Player = player;

        //gameSystem.ChangePhase(this.ToString());
    }

    public virtual IEnumerator Start()
    {
        yield break;
    }

    public virtual IEnumerator DrawCardFromDeck()
    {
        Debug.Log("Not allowed do draw card during this phase");
        yield break;
    }

    public virtual IEnumerator HatchDigitama()
    {
        Debug.Log("Not allowed to hatch Digitama during this phase");
        yield break;
    }

    public virtual IEnumerator PlayCard(GameObject card)
    {
        Debug.Log("Not allowed to move card to Battlezone during this phase");
        yield break;
    }

    public virtual IEnumerator Attack(GameObject attacker, GameObject target)
    {
        Debug.Log("Not allowed to attack during this phase");
        yield break;
    }

    public virtual IEnumerator CheckSecurity(GameObject attacker)
    {
        Debug.Log("Not allowed to attack security during this phase");
        yield break;
    }


    public virtual IEnumerator DirectAttack(GameObject attacker)
    {
        Debug.Log("Not allowed to attack security during this phase");
        yield break;
    }


    public virtual IEnumerator Skip()
    {
        yield break;
    }

    public virtual IEnumerator EndTurn()
    {
        //GameSystem.SetState(new EndPhase(GameSystem));
        yield break;
    }
}

