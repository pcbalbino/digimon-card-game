﻿using System.Collections;

internal class EndPhase : State
{
    public EndPhase(GameManager gameSystem, PlayerManager player) : base(gameSystem, player)
    {
    }

    public override IEnumerator Skip()
    {
        Player.SetState(new ActivePhase(GameSystem, Player));
        yield break;
    }
}