﻿using System.Collections;

internal class ActivePhase : State
{
    public ActivePhase(GameManager gameSystem, PlayerManager player) : base(gameSystem, player)
    {
    }

    public override IEnumerator Start()
    {
        //TODO Set Digimon to Active
        Player.SetState(new DrawPhase(GameSystem, Player));
        yield break;
    }
}