﻿using System;
using System.Collections;
using UnityEngine;

public class Begin : State
{
    public Begin(GameManager gameSystem, PlayerManager player) : base(gameSystem, player)
    {
    }

    public override IEnumerator Start()
    {
        yield return new WaitForSeconds(2f);

        Player.SetState(new RaisingPhase(GameSystem, Player));
    }
}