﻿using System.Collections;
using UnityEngine;

public class RaisingPhase : State
{
    public RaisingPhase(GameManager gameSystem, PlayerManager player) : base(gameSystem, player)
    {
    }
    public override IEnumerator HatchDigitama()
    {
        Player.CmdHatchEgg();
        Player.SetState(new MainPhase(GameSystem, Player));
        yield break;
    }

    public override IEnumerator PlayCard(GameObject card)
    {
        Player.CmdMoveRaisingCardToBattleArea(card);
        Player.SetState(new MainPhase(GameSystem, Player));

        yield break;
    }

    public override IEnumerator Skip()
    {
        Player.SetState(new MainPhase(GameSystem, Player));
        yield break;
    }
}