﻿using System;
using UnityEngine;
using Mirror;

public abstract class StateMachine : NetworkBehaviour
{
    protected State State;

    public void SetState(State state)
    {
        State = state;
        StartCoroutine(State.Start());
        Debug.Log(netId + " state changing to " + State.ToString());
    }

    public string GetState()
    {
        return State.ToString();
    }
}

