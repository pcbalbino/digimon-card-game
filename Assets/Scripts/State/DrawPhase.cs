﻿using System.Collections;
using UnityEngine;

internal class DrawPhase : State
{
    public DrawPhase(GameManager gameSystem, PlayerManager player) : base(gameSystem, player)
    {
    }

    public override IEnumerator Start()
    {
        //Player.CmdDrawCard();
        yield return new WaitForSeconds(1f);

        Player.SetState(new RaisingPhase(GameSystem, Player));
    }
}