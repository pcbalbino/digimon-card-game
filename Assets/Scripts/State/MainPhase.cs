﻿using System.Collections;
using UnityEngine;

public class MainPhase : State
{
    public MainPhase(GameManager gameSystem, PlayerManager player) : base(gameSystem, player)
    {
    }

    public override IEnumerator PlayCard(GameObject card)
    {
        Player.CmdPlaysCardFromHandToBattleArea(card);
        yield break;
    }

    public override IEnumerator Attack(GameObject attacker, GameObject target)
    {
        Player.CmdResolveAttack(attacker, target);
        yield break;
    }

    public override IEnumerator CheckSecurity(GameObject attacker)
    {
        Player.CmdPlayerChecksSecurity(attacker);
        yield break;
    }

    public override IEnumerator DirectAttack(GameObject attacker)
    {
        Player.CmdDirectAttack(attacker);
        yield break;
    }

    public override IEnumerator Skip()
    {
        Player.SetState(new EndPhase(GameSystem, Player));
        //Player.Pass();
        yield break;
    }
}