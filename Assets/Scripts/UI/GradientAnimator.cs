using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MPUIKIT;
using DG.Tweening;

public class GradientAnimator : MonoBehaviour
{

    MPImage image;
    GradientEffect gradientEffect;

    Gradient gradient;
    GradientColorKey[] colorKeys;
    GradientAlphaKey[] alphaKeys;

    Sequence sequence;

    // Start is called before the first frame update
    void Start()
    {
        image = GetComponent<MPImage>();
        gradientEffect = image.GradientEffect;

        gradient = gradientEffect.Gradient;
        colorKeys = gradient.colorKeys;
        alphaKeys = gradient.alphaKeys;

        sequence = DOTween.Sequence();

        Color aux = colorKeys[0].color;

        Tween tween1 = DOTween
            .To(
                () => colorKeys[0].color, col
             => colorKeys[0].color = col, colorKeys[1].color
                , 3f).SetEase(Ease.Linear);

        Tween tween2 = DOTween
            .To(
                () => colorKeys[1].color, col
             => colorKeys[1].color = col, aux
                , 3f).SetEase(Ease.Linear);


        sequence.Join(tween1);
        sequence.Join(tween2);

        sequence.OnUpdate(() =>
        {
            gradient.colorKeys = colorKeys;
            gradientEffect.Gradient = gradient;
            image.GradientEffect = gradientEffect;
        }).SetLoops(-1, LoopType.Yoyo).SetSpeedBased();
        //sequence.SetDelay(3f);
        sequence.Play();
    }

    void OnDestroy()
    {  
        sequence.Kill();
    }

}
