﻿using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;

public class UIEventBroker : NetworkBehaviour
{
    public static event Action<bool> RenderDirectAttackButton;
    public static event Action<bool> EndGame;

    public static void CallRenderDirectAttackButton(bool render)
    {
        if (RenderDirectAttackButton != null)
            RenderDirectAttackButton(render);
    }

    public static void CallEndGame(bool win){
        if(EndGame != null)
            EndGame(win);
    }
}
