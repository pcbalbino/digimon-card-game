﻿using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;

public class UIManager : NetworkBehaviour
{
    public GameObject p1Area;
    public GameObject p1Hand;
    // public SecurityAreaUI p1Security;

    void Start()
    {
        p1Area = GameObject.Find("Player_1_Area");
        p1Hand = p1Area.transform.FindDeepChild("Player_Hand").gameObject;
        // p1Security = p1Area.transform.FindDeepChild("Security_Area").gameObject.GetComponent<SecurityAreaUI>();

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
