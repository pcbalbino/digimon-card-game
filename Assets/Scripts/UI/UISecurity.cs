﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UISecurity : MonoBehaviour
{
    public GameObject p1Security;
    public GameObject p2Security;

    public int p1CurrentSecurityCount;
    public int p2CurrentSecurityCount;

    // Start is called before the first frame update
    void Start()
    {
        GameObject playerArea1 = GameObject.Find("Player_1_Area");
        p1Security = playerArea1.transform.FindDeepChild("Security_Area").gameObject;
        p1CurrentSecurityCount = 0;

        GameObject playerArea2 = GameObject.Find("Player_2_Area");
        p2Security = playerArea2.transform.FindDeepChild("Security_Area").gameObject;
        p2CurrentSecurityCount = 0;

        EventBroker.AddedCardToSecurity += AddCardToSecurity;
        EventBroker.ResolveSecurityCheck += RevealSecurity;
        EventBroker.ResolveSecurityBeingChecked += RevealSecurityBeingChecked;
    }

    private void AddCardToSecurity(GameObject card, bool isPlayer1)
    {
        card.GetComponent<CardUIManager>().ActivateSecurityView();
        if (isPlayer1)
        {
            Transform securitySlot = p1Security.transform.GetChild(p1CurrentSecurityCount);

            card.transform.SetPositionAndRotation(securitySlot.position, securitySlot.rotation);
            card.transform.SetParent(p1Security.transform.GetChild(p1CurrentSecurityCount));
            card.transform.localScale = new Vector3(1, 1, 1);
            card.tag = "SecurityArea";
            p1CurrentSecurityCount++;
        }
        else
        {
            Transform securitySlot = p2Security.transform.GetChild(p2CurrentSecurityCount);

            card.transform.SetPositionAndRotation(securitySlot.position, securitySlot.rotation);
            card.transform.SetParent(p2Security.transform.GetChild(p2CurrentSecurityCount));
            card.transform.localScale = new Vector3(1, 1, 1);
            card.tag = "SecurityArea";
            p2CurrentSecurityCount++;
        }
    }


    public void RevealSecurity(GameObject attacker, GameObject securityCard, Enums.BATTLE_OUTCOMES result)
    {
        if (p2CurrentSecurityCount <= 0) return;

        securityCard.GetComponent<CardUIManager>().ActivateHandView();
        securityCard.GetComponent<Animator>().SetTrigger("Highlight");
    }

    public void RevealSecurityBeingChecked(GameObject attacker, GameObject securityCard, Enums.BATTLE_OUTCOMES result)
    {
        if (p1CurrentSecurityCount <= 0) return;

        securityCard.GetComponent<CardUIManager>().ActivateHandView();
        securityCard.GetComponent<Animator>().SetTrigger("Highlight");
    }
}
