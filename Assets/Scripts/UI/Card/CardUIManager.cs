﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Mirror;

public class CardUIManager : MonoBehaviour
{
    private BasicCard basicCard;

    [SerializeField] private GameObject battleView;
    [SerializeField] private GameObject handView;
    [SerializeField] private GameObject securityView;

    private void Awake()
    {
        basicCard = GetComponent<BasicCard>();
        // Subscribe when reveals security
    }

    public virtual void ActivateHandView()
    {
        handView.SetActive(true);
        battleView.SetActive(false);
        securityView.SetActive(false);

        // if(hasA == false)
        //     battleView.GetComponent<CardUI>().enabled = false;
    }

    public virtual void ActivateP2HandView()
    {
        ActivateHandView();
        handView.GetComponent<CardUI>().enabled = false;
        battleView.GetComponent<CardUIBattle>().enabled = false;
    }

    public virtual void ActivateBattleView()
    {
        battleView.SetActive(true);
        handView.SetActive(false);
        securityView.SetActive(false);
    }

    public virtual void ActivateP2BattleView()
    {
        ActivateBattleView();
        battleView.GetComponent<CardUIBattle>().enabled = false;
    }

    public virtual void ActivateSecurityView()
    {
        battleView.SetActive(false);
        handView.SetActive(false);
        securityView.SetActive(true);
    }

    internal void CancelCardMovement()
    {
        handView.GetComponentInChildren<CardUI>().ReturnToOriginalPosition();
    }

    #region CARD APPEARANCE
    public void LoadCardValues(BasicCard original)
    {
        //gameObject.name = original.GetInstanceID().ToString();
        basicCard.cardName = original.cardName;
        basicCard.DP = original.DP;
        basicCard.memoryCost = original.memoryCost;
        basicCard.level = original.level;
        basicCard.cardID = original.cardID;
    }

    public virtual void UpdateAllTagsOfCard(GameObject card, string newTag)
    {
        foreach (Transform t in card.transform)
        {
            UpdateAllTagsOfCard(t.gameObject, newTag);
            t.gameObject.tag = newTag;
        }
    }
    #endregion
}