﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Mirror;

public class CardUI : NetworkBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    private static string CARDS_PATH = "Sprites/Cards/ST1/";
    private Vector3 previousPosition;
    private BasicCard basicCard;

    [SerializeField] private GameObject handView;
    [SerializeField] private Text dPText;
    [SerializeField] private Text lvText;
    [SerializeField] private Text costText;

    private void Awake()
    {
        basicCard = GetComponentInParent<BasicCard>();
        //Player = NetworkClient.connection.identity.GetComponent<LegacyPlayerManager>();
    }

    private void OnEnable()
    {
        LoadCardUI();
    }

    #region DRAG HANDLERS
    public void OnBeginDrag(PointerEventData eventData)
    {
        SaveSourcePosition(eventData);
    }

    public void OnDrag(PointerEventData eventData)
    {
        UpdatePosition(eventData);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        MoveCard(eventData.pointerEnter);
    }
    #endregion

    #region MOVEMENT HANDLERS
    private void MoveCard(GameObject areaBeingDraggedTo)
    {
        Debug.Log(netId + this.name + " area dragged to " + areaBeingDraggedTo.name);
        if (areaBeingDraggedTo.CompareTag(Enums.AREA_BATTLE))
        {
            if (this.CompareTag(Enums.AREA_RAISING))
                EventBroker.CallsPlayerMovesRaisingCardToBattleArea(transform.parent.gameObject);
            else
            {
                EventBroker.CallsPlayerPlaysCardFromHandToBattleArea(transform.parent.gameObject);
            }
            //Player.MoveRaisingDigimonToBattleArea();
            //Player.MoveCardIntoBattleArea(gameObject.transform.parent.gameObject);
        }
        else
        {
            ReturnToOriginalPosition();
        }
    }

    private void UpdatePosition(PointerEventData eventData)
    {
        Vector3 tempFingerPosition = Camera.main.ScreenToWorldPoint(new Vector3(eventData.position.x, eventData.position.y, transform.position.z - Camera.main.transform.position.z));
        transform.position = tempFingerPosition;
    }

    private void SaveSourcePosition(PointerEventData eventData)
    {
        previousPosition = eventData.pointerDrag.transform.localPosition;
    }

    internal void ReturnToOriginalPosition()
    {
        this.transform.localPosition = previousPosition;
    }

    #endregion

    #region CARD APPEARANCE
    public void LoadCardUI()
    {
        Sprite cardView = Resources.Load<Sprite>(CARDS_PATH + basicCard.cardID);
        GetComponentInChildren<Image>().sprite = cardView;

        dPText.text = basicCard.DP.ToString();
        int level = (int)basicCard.level;
        lvText.text = "Lv." + level;
        costText.text = basicCard.memoryCost.ToString();
    }
    #endregion
}
