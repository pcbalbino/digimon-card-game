﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Mirror;

public class CardUIBattle : NetworkBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    private static string CARDS_PATH = "Sprites/Cards/ST1/";

    [SerializeField]
    private Text dPText;
    [SerializeField]
    private Text lvText;

    [SerializeField]
    private GameObject linePrefab;
    private BasicCard basicCard;
    private GameObject currentLine;
    private LineRenderer lineRenderer;

    //TODO Remove
    [SerializeField]
    public float arrowHeadSize;

    private void Awake()
    {
        basicCard = GetComponentInParent<BasicCard>();
        LoadCardUI();
    }

    #region DRAG HANDLERS
    public void OnBeginDrag(PointerEventData eventData)
    {
        //TODO deactivate any other interaction
        CreateLine();

        //TODO Direct Attack posibility
        UIEventBroker.CallRenderDirectAttackButton(true);
    }

    public void OnDrag(PointerEventData eventData)
    {
        Vector3 tempFingerPosition = Camera.main.ScreenToWorldPoint(new Vector3(eventData.position.x, eventData.position.y, transform.position.z + 1));

        if (Vector3.Distance(tempFingerPosition, lineRenderer.GetPosition(0)) > 1f)
            UpdateLine(tempFingerPosition);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        Debug.Log(eventData.pointerEnter.tag + " " + eventData.pointerEnter.name);

        GameObject attacker = null;
        GameObject aux = gameObject;

        // Finds the parent gameobject that holds all cards
        while(attacker == null && !aux.name.Equals("Canvas") && aux.transform.parent != null){
            if(aux.GetComponent<CardUIManager>() != null){
                attacker = aux;
            }
            else{
                aux = aux.transform.parent.gameObject;
            }
        }

        // Finds the parent gameobject that holds all cards
        GameObject target = null;
        aux =  eventData.pointerEnter;
        while(target == null && !aux.name.Equals("Canvas") && aux.transform.parent != null){
            if(aux.GetComponent<CardUIManager>() != null || aux.name.Equals("Direct_Attack"))
                target = aux;
            else{
                aux = aux.transform.parent.gameObject;
            }
        }

        if((attacker != null && target != null) && (attacker != target)){
            if(target.tag.Equals("SecurityArea"))
                EventBroker.CallPlayerChecksSecurity(attacker);
            else if(target.transform.parent.name.Equals("Battle_Area") && !target.transform.parent.parent.name.Equals("Battle_Area"))
                EventBroker.CallPlayerAttacksCard(attacker, target);
            else if(target.name.Equals("Direct_Attack"))
                EventBroker.CallPlayerDirectAttacks(attacker);
        }
        else
            Debug.Log(netId + this.name + "attacking or target card holder could not be found. Attacker was " +gameObject.name
                +" and target was " + eventData.pointerEnter.name);

        UIEventBroker.CallRenderDirectAttackButton(false);
        Destroy(currentLine);
    }
    #endregion

    #region CARD APPEARANCE
    public void LoadCardUI()
    {
        Texture cardView = Resources.Load<Texture>(CARDS_PATH + basicCard.cardID);
        GetComponentInChildren<RawImage>().texture = cardView;

        dPText.text = basicCard.DP.ToString();
        int level = (int)basicCard.level;
        lvText.text = level.ToString();
    }
    #endregion

    #region EXECUTION
    void CreateLine()
    {
        currentLine = Instantiate(linePrefab);
        lineRenderer = currentLine.GetComponent<LineRenderer>();

        lineRenderer.positionCount = 2;
        lineRenderer.SetPosition(0, new Vector3(transform.position.x, transform.position.y, transform.position.z +1));
    }

    void UpdateLine(Vector3 newFingerPosition)
    {
        lineRenderer.SetPosition(1, newFingerPosition);
    }
    #endregion
}
