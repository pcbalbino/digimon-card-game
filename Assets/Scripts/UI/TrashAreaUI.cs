﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TrashAreaUI : MonoBehaviour
{
    private Transform p1DiscardPile;
    private Transform p2DiscardPile;

    private void Start()
    {
        EventBroker.ResolveAttack += ResolveAttack;
        EventBroker.ResolveDefence += ResolveDefence;
        EventBroker.ResolveSecurityCheck += RevealSecurity;
        EventBroker.ResolveSecurityBeingChecked += RevealSecurityBeingChecked;


        GameObject p1Area = GameObject.Find("Player_1_Area");
        GameObject p2Area = GameObject.Find("Player_2_Area");

        try
        {
            if (p1Area != null)
            {
                p1DiscardPile = p1Area.transform.FindDeepChild("Trash_Area").GetChild(0);
            }
            if (p2Area != null)
            {
                p2DiscardPile = p2Area.transform.FindDeepChild("Trash_Area").GetChild(0);
            }
        }
        catch (NullReferenceException)
        {
            Debug.LogError("Could not find players hands... ");
        }
    }

    public void ResolveAttack(GameObject attacker, GameObject target, Enums.BATTLE_OUTCOMES result)
    {
        if (Enums.BATTLE_OUTCOMES.WIN.Equals(result))
        {
            StartCoroutine(WaitForAnimation(target, p2DiscardPile));
        }
        else if (Enums.BATTLE_OUTCOMES.LOSE.Equals(result))
        {
            StartCoroutine(WaitForAnimation(attacker, p1DiscardPile));
        }
        else if (Enums.BATTLE_OUTCOMES.TIE.Equals(result))
        {
            StartCoroutine(WaitForAnimation(attacker, p1DiscardPile));
            StartCoroutine(WaitForAnimation(target, p2DiscardPile));
        }
    }

    public void ResolveDefence(GameObject attacker, GameObject target, Enums.BATTLE_OUTCOMES result)
    {
        if (Enums.BATTLE_OUTCOMES.WIN.Equals(result))
        {
            StartCoroutine(WaitForAnimation(target, p1DiscardPile));
        }
        else if (Enums.BATTLE_OUTCOMES.LOSE.Equals(result))
        {
            StartCoroutine(WaitForAnimation(attacker, p2DiscardPile));
        }
        else if (Enums.BATTLE_OUTCOMES.TIE.Equals(result))
        {
            StartCoroutine(WaitForAnimation(target, p1DiscardPile));
            StartCoroutine(WaitForAnimation(attacker, p2DiscardPile));
        }
    }

    public void RevealSecurity(GameObject attacker, GameObject securityCard, Enums.BATTLE_OUTCOMES result)
    {
        if (Enums.BATTLE_OUTCOMES.LOSE.Equals(result) || Enums.BATTLE_OUTCOMES.TIE.Equals(result))
        {
            StartCoroutine(WaitForAnimation(attacker, p1DiscardPile));
        }
        StartCoroutine(WaitForAnimation(securityCard, p2DiscardPile));
    }

    public void RevealSecurityBeingChecked(GameObject attacker, GameObject securityCard, Enums.BATTLE_OUTCOMES result)
    {
        //IF NOT SEND TO BATTLE AREA
        if (Enums.BATTLE_OUTCOMES.LOSE.Equals(result) || Enums.BATTLE_OUTCOMES.TIE.Equals(result))
        {
            StartCoroutine(WaitForAnimation(attacker, p2DiscardPile));
        }
        StartCoroutine(WaitForAnimation(securityCard, p1DiscardPile));
    }

    private IEnumerator WaitForAnimation(GameObject card, Transform pile)
    {
        yield return new WaitForSeconds(1.5f);

        card.transform.SetParent(pile);
        card.transform.SetPositionAndRotation(pile.position, pile.rotation);

        card.transform.GetChild(0).tag = Enums.AREA_TRASH;
        card.tag = Enums.AREA_TRASH;
        yield break;
    }
}
