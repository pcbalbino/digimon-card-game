﻿using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;

public class UIHand : MonoBehaviour
{
    public GameObject player1Area;
    public GameObject p1Hand;
    public GameObject p1Deck;
    public GameObject player2Area;
    public GameObject p2Hand;
    public GameObject p2Deck;

    void Start(){
        player1Area = GameObject.Find("Player_1_Area");
        player2Area = GameObject.Find("Player_2_Area");

        EventBroker.AddedCardToHand += AddCardToHand;

        try{
            if(player1Area != null){
                p1Hand = player1Area.transform.FindDeepChild("Player_Hand").gameObject;
                p1Deck = player1Area.transform.FindDeepChild("Deck_Area").gameObject;
            }

            if(player2Area != null){
                p2Hand = player2Area.transform.FindDeepChild("Player_Hand").gameObject;
                p2Deck = player2Area.transform.FindDeepChild("Deck_Area").gameObject;
            }

        }catch(NullReferenceException){
            Debug.LogError("Could not find players hands: ");
        }
    }

    private void AddCardToHand(GameObject card, bool isPlayer1){
        
        if(isPlayer1){
            card.transform.SetParent(p1Deck.transform, false);
            card.GetComponent<CardUIManager>().ActivateHandView();

            LeanTween.move(card, p1Hand.transform.position, 0.3f)
                .setOnComplete(() =>
                {
                    card.transform.SetParent(p1Hand.transform, false);
                });
        }else{
            card.transform.SetParent(player2Area.transform, false);
            card.GetComponent<CardUIManager>().ActivateSecurityView();

            LeanTween.move(card, p2Hand.transform.position, 0.3f)
                .setOnComplete(() =>
                {
                    card.transform.SetParent(p2Hand.transform, false);
                });
        }
    }

}
