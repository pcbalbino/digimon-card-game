﻿using UnityEngine;
using UnityEngine.UI;
using Mirror;
using System;

public class RaisingAreaUI : MonoBehaviour
{
    public GameObject p1RaisingDeck;
    public GameObject p1BabyHolder;

    public GameObject p2RaisingDeck;
    public GameObject p2BabyHolder;

    void Start()
    {
        GameObject p1Area = GameObject.Find("Player_1_Area");
        GameObject p2Area = GameObject.Find("Player_2_Area");

        EventBroker.HatchedEgg += PlaceDigimonInRaisingArea;
        EventBroker.AddedRaisingCardToBattleArea += ActivateHatchingEggs;

        try
        {
            if (p1Area != null)
            {
                p1BabyHolder = p1Area.transform.FindDeepChild("Baby_Holder").gameObject;
                p1RaisingDeck = p1Area.transform.FindDeepChild("Digitama_Deck").gameObject;
            }
            if (p2Area != null)
            {
                p2BabyHolder = p2Area.transform.FindDeepChild("Baby_Holder").gameObject;
                p2RaisingDeck = p2Area.transform.FindDeepChild("Digitama_Deck").gameObject;
            }
        }
        catch (NullReferenceException)
        {
            Debug.LogError("Could not find players hands... ");
        }
    }

    public void HatchEgg()
    {
        EventBroker.CallPlayerHatchesEgg();
    }

    public void PlaceDigimonInRaisingArea(GameObject card, bool isPlayer1)
    {
        if (isPlayer1)
        {
            if (p1BabyHolder.transform.childCount == 1)
                return;

            card.transform.SetPositionAndRotation(p1BabyHolder.transform.position, p1BabyHolder.transform.rotation);
            card.transform.SetParent(p1BabyHolder.transform);

            card.transform.localScale = new Vector3(1, 1, 1);

            card.GetComponent<CardUIManager>().ActivateHandView();
            card.GetComponent<CardUIManager>().UpdateAllTagsOfCard(card, Enums.AREA_RAISING);

            DeactivateHatchingEggs();
        }
        else
        {
            if (p2BabyHolder.transform.childCount == 1)
                return;

            card.transform.SetPositionAndRotation(p2BabyHolder.transform.position, p2BabyHolder.transform.rotation);
            card.transform.SetParent(p2BabyHolder.transform);

            card.transform.localScale = new Vector3(1, 1, 1);

            card.GetComponent<CardUIManager>().ActivateP2HandView();
            card.GetComponent<CardUIManager>().UpdateAllTagsOfCard(card, Enums.AREA_RAISING);
        }
    }

    internal void DeactivateHatchingEggs()
    {
        p1RaisingDeck.GetComponent<Button>().interactable = false;
    }

    internal void ActivateHatchingEggs(GameObject card, bool isPlayer1)
    {
        p1RaisingDeck.GetComponent<Button>().interactable = true;
    }


}
