﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mirror;

public class DeckUI : MonoBehaviour
{
    private Text player1DeckText;
    public void Start()
    {
        player1DeckText = GetComponentInChildren<Text>();
    }

    public void UpdateRemainingCards(int remaningCards)
    {
        string aux = remaningCards.ToString();
        player1DeckText.text = aux;
    }
}
