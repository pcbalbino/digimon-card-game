﻿using UnityEngine;
using System;

public class BattlezoneUI : MonoBehaviour
{
    public GameObject p1BattleArea;
    public GameObject p2BattleArea;

    public GameObject directAttack;
    void Start()
    {
        EventBroker.AddedRaisingCardToBattleArea += PlayCard;
        EventBroker.PlayedCardFromHandToBattleArea += PlayCard;
        UIEventBroker.RenderDirectAttackButton += RenderDirectAttackButton;

        GameObject p1Area = GameObject.Find("Player_1_Area");
        GameObject p2Area = GameObject.Find("Player_2_Area");

        try
        {
            if (p1Area != null)
            {
                p1BattleArea = p1Area.transform.FindDeepChild("Battle_Area").gameObject;
            }
            if (p2Area != null)
            {
                p2BattleArea = p2Area.transform.FindDeepChild("Battle_Area").gameObject;
                directAttack = p2Area.transform.FindDeepChild("Direct_Attack").gameObject;
            }
        }
        catch (NullReferenceException)
        {
            Debug.LogError("Could not find players hands... ");
        }
    }
    public void PlayCard(GameObject card, bool isPlayer1)
    {
        if (isPlayer1)
        {
            card.GetComponent<CardUIManager>().ActivateBattleView();
            card.transform.SetParent(p1BattleArea.transform, false);
            card.GetComponent<Animator>().SetTrigger("Highlight");
        }
        else
        {
            card.GetComponent<CardUIManager>().ActivateP2BattleView();
            card.transform.SetParent(p2BattleArea.transform, false);
            card.GetComponent<Animator>().SetTrigger("Highlight");
        }
    }

    public void RenderDirectAttackButton(bool render){
        Transform securityCard = GameObject.Find("Player_2_Area").transform.FindDeepChild("Security_Area").Find("Security_1");

        if(securityCard.childCount == 0)
            directAttack.SetActive(render);

    }
}
