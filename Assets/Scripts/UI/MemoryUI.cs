﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MemoryUI : MonoBehaviour
{
    private static int ARRAY_MEMORY_FIXER = 10;

    [SerializeField]
    private GameObject memoryCounter;
    [SerializeField]
    private GameObject memoryHolder;

    void Start(){
        EventBroker.UsedMemory += UpdateMemoryCounter;
    }
    /*
     * +10 is added to currentMemory to match transform array
     */
    public void UpdateMemoryCounter(int currentMemory)
    {
        Debug.Log(currentMemory);
        memoryCounter.transform.position = memoryHolder.transform.GetChild(currentMemory + ARRAY_MEMORY_FIXER).position;
    }
}
