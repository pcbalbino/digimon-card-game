﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class BasicCard : NetworkBehaviour
{
    [SerializeField]
    [SyncVar]
    public string cardName;
    [SerializeField]
    [SyncVar]
    public int memoryCost;
    [SerializeField]
    [SyncVar]
    public string cardID;
    [SerializeField]
    [SyncVar]
    public COLOUR colour;
    [SerializeField]
    [SyncVar]
    public TYPE type;
    [SerializeField]
    [SyncVar]
    public RARITY rarity;
    [SerializeField]
    [SyncVar]
    public LEVEL level;
    [SerializeField]
    [SyncVar]
    public SPECIES species;
    [SerializeField]
    [SyncVar]
    public ATTRIBUTE attribute;
    [SerializeField]
    [SyncVar]
    public int DP;

    public enum COLOUR { NONE, RED, BLUE, GREEN, YELLOW };
    public enum TYPE { DIGIMON, DIGITAMA, OPTION, TAMER };
    public enum RARITY { NONE, COMMON, UNCOMMON, RARE, SUPERRARE };
    public enum LEVEL { NONE, DIGITAMA, BABY, ROOKIE, CHAMPION, ULTIMATE, MEGA };
    public enum SPECIES
    {
        NONE, ANIMAL, DRAGON, MAMMAL, DINOSAUR, BIRD, GIANT_BIRD, CYBORG, DRAGON_MAN, BIRD_MAN
    };
    public enum ATTRIBUTE { NONE, VACCINE, DATA, VIRUS }
}
