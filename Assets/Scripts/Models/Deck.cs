﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Deck : MonoBehaviour
{
    //[SerializeField]
    private List<GameObject> cards = new List<GameObject>();
    [SerializeField]
    private List<GameObject> prefabs = new List<GameObject>();

    public void Awake()
    {
        FillDeck();
        Shuffle();
    }

    public void FillDeck()
    {
        foreach(GameObject prefab in prefabs)
        {
            cards.Add(Instantiate(prefab));
        }
    }

    /*
     *   This methods shuffles the order of 
     *      It implements Knuth shuffle algorithm
     */
    public void Shuffle()
    {
        for(int i=0; i< cards.Count; i++)
        {
            GameObject aux = cards[i];
            int r = Random.Range(i, cards.Count);
            cards[i] = cards[r];
            cards[r] = aux;
        }
    }

    /*
     * Draws one card from the deck and reduces its size  
     */  
    public GameObject DrawCard()
    {
        GameObject cardToDraw = cards[cards.Count-1];
        cards.RemoveAt(cards.Count - 1);

        return cardToDraw;
    }

    public int GetCurrentLength()
    {
        return cards.Count;
    }

    public string LogCards(){
        return cards.ToString();
    }
}
