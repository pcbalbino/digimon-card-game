﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Mirror;
using UnityEngine;
using UnityEngine.TestTools;

public class DeckV2
{
    public List<GameObject> cardsList { get; private set; }
    public GameObject deckPrefab { get; private set; }

    public GameObject cardPrefab { get; private set; }
    public GameObject deck;

    public DeckV2(){}

    public DeckV2(GameObject deckPrefab, GameObject cardPrefab)
    {
        cardsList = new List<GameObject>();
        this.deckPrefab = deckPrefab;
        this.cardPrefab = cardPrefab;
    }

    #region Deck
    public void LoadDeck()
    {
        deck = GameObject.Instantiate(deckPrefab);

        if (deck.GetComponentsInChildren<Transform>() == null)
            throw new ArgumentException("Deck prefab without cards " + deck);

        foreach (Transform cardTransform in deck.GetComponentsInChildren<Transform>())
        {
            if (cardTransform.gameObject.GetComponent<BasicCard>() == null)
                continue;//throw new ArgumentException("Deck prefab contains invalid cards " + cardTransform);

            GameObject card = GameObject.Instantiate(cardPrefab);
            card.GetComponent<CardUIManager>().LoadCardValues(cardTransform.gameObject.GetComponentInChildren<BasicCard>());
            cardsList.Add(card.gameObject);
        }

        ShuffleDeck();
    }

    public void ShuffleDeck()
    {
        var random = new System.Random();
        cardsList = cardsList.OrderBy(x => random.Next()).ToList();
    }

    public virtual GameObject DrawCard()
    {
        GameObject card = cardsList[cardsList.Count - 1];
        cardsList.Remove(card);

        EventBroker.CallCardDrawn(cardsList.Count);

        return card;
    }
    #endregion
}
