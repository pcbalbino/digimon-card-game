using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using System.IO;

public static class SaveSystem
{
    public static void SaveDeck(DeckV3 deck)
    {
        string path = Application.persistentDataPath;
        System.IO.File.WriteAllText( Application.persistentDataPath + "/" + deck.name + ".json", JsonUtility.ToJson(deck));
    }

    public static DeckV3 LoadDeck(){
        string [] files = System.IO.Directory.GetFiles(Application.persistentDataPath);

        DeckV3 deckSaved = new DeckV3();

        foreach(string file in files)
        {
            deckSaved = JsonUtility.FromJson<DeckV3>(System.IO.File.ReadAllText(file));
            return deckSaved;
        }
        return null;
    }
}
