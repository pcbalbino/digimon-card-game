﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enums
{
    public static readonly string AREA_SECURITY = "SecurityArea";
    public static readonly string AREA_BATTLE = "BattleArea";
    public static readonly string AREA_RAISING = "RaisingArea";
    public static readonly string AREA_TRASH = "TrashArea";

    public enum BATTLE_OUTCOMES{
        WIN,
        LOSE,
        TIE
    }

    public enum SKILL_KIND
    {
        NONE,
        ATTACK,
        AUTO,
        MANUAL,
        BLOCKER,
        SECURITY
    }

    public enum SKILL_TYPE
    {
        NONE,
        DP_BUFF,
        DP_DEBUFF,
        DP_BUFF_FIELD,
        DP_DEBUFF_FIELD,
        DP_BUFF_SECURITY,
        DP_DEBUFF_SECURITY,
        DP_BUFF_IN_BATTLE,
        DP_DEBUFF_ADDITIONAL, // WHATT?
        DRAW_CARD,
        BLOCKER,
        FORCE_STATE_CHANGE,
        FORCE_STATE_CHANGE_ONESHOT,
        PEEPING_CARD,
        DISCARD_CARD,
        DESTROY,
        DESTROY_EVO_SOURCE,
        EVOLUTION,
        DEGENERATION,
        SECURITY_ATTACK_BUFF,
        SECURITY_ATTACK_DEBUFF,
        SECURITY_ATTACK_TAMER,
        SECURITY_ATTACK_EVO_SOURCE,
        SECURITY_ATTACK_BUFF_FIELD,
        SECURITY_ATTACK_DEBUFF_FIELD,
        RECOVER,
        FIGHTING_ABILITY,
        COST_CHANGE,
        NEGATE_ACTION,
        GRANT_SHILL,
        TOPDECK_EVOLUTION,
        TOPDECK_DP_BUFF,
        MEMORY_CHANGE,
        MEMORY_CHANGE_ONESHOT,
        PUT_INTO_CARD,
        RESTRICTION,
        ADD_HAND_CARD,
        SECURITY_ATTACK_BUFF_NO_EVOSRC
    }
    public enum TIMING_TYPE
    {
        NONE,
        MYSELF_ATTACKED,
        ATTACK_EITHER,
        ATTACK_PLAYER,
        ATTACK_ENEMY,
        PLAYED,
        APPEAR,
        EVOLVED,
        IN_AREA,
        IN_HAND,
        MYSELF_WON_BATTLE,
        MYSELF_BLOCKED,
        MYSELF_TURN,
        MYSELF_TURN_ACTIVE,
        ENEMY_TURN,
        MYSELF_TARGETED,
        NO_SRC_DIGIMON_ATTACKING,
        BATTLE_TO_NO_SRC_DIGIMON
    }

    public enum CONDITION_TYPE
    {
        NONE,
        ATTACKER_HAD_BLOCKER_ABILITY,
        MYSELF_ATTACK,
        MYSELF_ACTIVE,
        MYSELF_REST,
        MYSELF_IN_AREA,
        MYSELF_IN_HAND,
        MYSELF_OPERATOR_TURN,
        MYSELF_EVO_SRC_4_OR_MORE,
        MYSELF_ENEMY_DP_DEBUFF,
        EXIST_PLAYER_DIGIMON,
        EXIST_PLAYER_BLOCKER,
        EXIST_PLAYER_TAMER,
        EXIST_PLAYER_SECURITY,
        EXIST_PLAYER_NO_EVO_SRC_DIGIMON,
        EXIST_PLAYER_EVO_SRC_DIGIMON,
        EXIST_PLAYER_EVO_SRC_OF_UNDER_COMPLETE,
        EXIST_ENEMY_DIGIMON,
        EXIST_ENEMY_BLOCKER,
        EXIST_ENEMY_TAMER,
        EXIST_ENEMY_SECURITY,
        EXIST_ENEMY_NO_EVO_SRC_DIGIMON,
        EXIST_ENEMY_EVO_SRC_DIGIMON,
        EXIST_ENEMY_EVO_SRC_OF_UNDER_COMPLETE,
        EXIST_ENEMY_DIGIMON_VANISHED,
        MYSELF_IN_AREA_AND_EXIST_ENEMY_NO_EVO_SRC
    }

    public enum TARGET_TYPE
    {
        NONE,
        NO_TARGET,
        MYSELF,
        ATTACKER,
        EACH_HAND,
        EACH_DECK,
        ALL_DIGIMON,
        ALL_SECURITY,
        ALL_SRC_DIGIMON,
        PLAYER_DIGIMON,
        PLAYER_SRC_DIGIMON,
        PLAYER_SECURITY,
        PLAYER_HAND,
        PLAYER_DECK,
        PLAYER_ALL_DIGIMON,
        PLAYER_ALL_SRC_DIGIMON,
        PLAYER_ALL_SECURITY,
        PLAYER_ALL_SECURITY_DIGIMON,
        PLAYER_ALL_HAND,
        PLAYER_ALL_DECK,
        PLAYER_FIELD,
        ENEMY_DIGIMON,
        ENEMY_DIGIMON_DP_DEBUFF_TARGET,
        ENEMY_NO_SRC_DIGIMON,
        ENEMY_SRC_DIGIMON,
        ENEMY_LOWER_DIGIMON_SRC,
        ENEMY_SECURITY,
        ENEMY_HAND,
        ENEMY_DECK,
        ENEMY_ALL_DIGIMON,
        ENEMY_ALL_SRC_DIGIMON,
        ENEMY_ALL_SECURITY,
        ENEMY_ALL_HAND,
        ENEMY_ALL_DECK,
        ENEMY_FIELD
    }

    public enum SKLL_PATTERN
    {
        MAIN_SKILL,
        EVO_SOURCE_SKILL,
        SKILL_PRN_NUM
    }
}
