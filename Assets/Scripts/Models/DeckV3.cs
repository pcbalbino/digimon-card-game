using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DeckV3
{
    public string name;
    public string[] cards;
    public string[] digitamaCards;

    public DeckV3(){
        name = "";
        cards = new string [50];
        digitamaCards = new string [5];
    }

    public DeckV3(string n, string[] c, string[] dc){
        name = n;
        cards = c;
        digitamaCards = dc;
    }

    public DeckV3 (DeckV3 deck)
    {
        name = deck.name;
        cards = deck.cards;
        digitamaCards = deck.digitamaCards;
    }

    public void LoadDeck(){
        
    }
}
