﻿using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;

public class Player
{
    public GameObject deckPrefab { get; set; }
    public GameObject digitamaDeckPrefab { get; set; }
    public DeckV2 deck { get; set; }
    public DeckV2 digitamaDeck { get; set; }

    public GameObject raisingCard { get; private set; }

    public List<GameObject> hand { get; private set; }
    public List<GameObject> security { get; private set; }
    public List<GameObject> battleCards { get; private set; }
    public List<GameObject> tamers { get; private set; }
    public List<GameObject> discardPile {get; private set;}

    public Player()
    {
        OnStart();
    }

    void OnStart()
    {
        deck = new DeckV2(Resources.Load<GameObject>("Prefabs/Decks/Sample_Deck"),Resources.Load<GameObject>("Prefabs/Cards/BasicCard"));
        digitamaDeck = new DeckV2(Resources.Load<GameObject>("Prefabs/Decks/Sample_Deck"), Resources.Load<GameObject>("Prefabs/Cards/BasicCard"));

        deck.LoadDeck();
        digitamaDeck.LoadDeck();

        hand = new List<GameObject>();
        security = new List<GameObject>();
        battleCards = new List<GameObject>();
        tamers = new List<GameObject>();
        discardPile = new List<GameObject>();
    }

    public void StartGame()
    {
        // Loads 5 cards to hand and 5 securities
        for (int i = 0; i < 5; i++)
        {
            hand.Add(DrawCard());
            security.Add(DrawCard());
        }
        
    }

    public void AddSecurity(GameObject card){
        // if(card == null || card.GetComponentInChildren<BasicCard>() == null)
        //     throw new ArgumentException("Adding non card to security area " + card);
        security.Add(card);
    }

    public void CheckSecurity(GameObject card){
        if(security.Count == 0)
            throw new ArgumentOutOfRangeException("There are no more security cards.");
        if(!security.Contains(card))
            throw new ArgumentException("Card is not present in the security cards");
        security.Remove(card);
    }

    public GameObject CheckSecurity(){
        if(security.Count == 0){
            Debug.LogError("There are no more security cards.");
            return null;
        }
        
        GameObject secCard = security[security.Count-1];
        security.Remove(secCard);
        return secCard;
    }

    public void AddCardToBattleArea(GameObject card){
        if(card == null || card.GetComponentInChildren<BasicCard>() == null)
            return;

        battleCards.Add(card);

        if(card.Equals(raisingCard))
        {
            raisingCard = null;
        }
    }

    public void RemoveCardFromBattleArea(GameObject card){
        if(battleCards.Count == 0)
            throw new ArgumentOutOfRangeException("There are no cards in the battle area.");
        if(!battleCards.Contains(card))
            throw new ArgumentException("Card is not present in the battle area");
        battleCards.Remove(card);
    }

    public GameObject DrawCard(){
        //GameObject prefabCard =  deck.DrawCard();
        //GameObject card = GameObject.Instantiate(prefabCard);
        GameObject card = deck.DrawCard();
        // NetworkServer.Spawn(card, NetworkClient.connection);
        return card;
    }

    public void AddCardToHand(GameObject card){
        if(card == null || card.GetComponentInChildren<BasicCard>() == null)
            throw new ArgumentException("Adding non card object to hand " + card);
        hand.Add(card);
    }

    public void RemoveCardFromHand(GameObject card){
        if(hand.Count == 0)
            throw new ArgumentOutOfRangeException("There are no cards in hand.");
        if(!hand.Contains(card))
            throw new ArgumentException("Card is not present in Hand.");
        hand.Remove(card);
    }

    public GameObject HatchEgg(){
        if(raisingCard != null)
            throw new InvalidOperationException("Trying to hatch an egg with an exisiting raising Digimon");
        raisingCard = digitamaDeck.DrawCard();
        return raisingCard;
    }

    public void MoveRaisingCardToBattleArea(){
        if(raisingCard == null)
            throw new InvalidOperationException("Trying to move raising card to battle are without raising card");
        battleCards.Add(raisingCard);
        raisingCard = null;
    }

    public void AddToDiscardPile(GameObject card){
        if(card == null || card.GetComponentInChildren<BasicCard>() == null)
            throw new ArgumentException("Trying to discard invalid card or object: " + card );
        discardPile.Add(card);
    }

    public void AddTamerToBattleArea(GameObject card){
        if(card == null || card.GetComponentInChildren<BasicCard>() == null)
            throw new ArgumentException("Trying to discard invalid card or object: " + card );
        if(!card.GetComponent<BasicCard>().type.Equals(BasicCard.TYPE.TAMER))
            throw new ArgumentException("Trying to add non-tamer card to tamer area: " + card);
        tamers.Add(card);
    }
    
}