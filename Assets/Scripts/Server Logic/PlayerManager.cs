﻿using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;

public class PlayerManager : StateMachine
{
    public Player player;
    public GameManager GameManager;
    public int memory = 0;

    #region Setup
    public override void OnStartClient()
    {
        base.OnStartClient();
        GameManager = FindObjectOfType<GameManager>();

        if (!hasAuthority)
            return;

        EventBroker.PlayerHatchesEgg += PlayerHatchesEgg;
        EventBroker.PlayerMovesRaisingCardToBattleArea += PlayerMovesRaisingCardToBattleArea;
        EventBroker.PlayerPlaysCardFromHandToBattleArea += PlayerPlaysCardFromHandToBattleArea;
        EventBroker.PlayerAttacksCard += PlayerAttacksCard;
        EventBroker.PlayerChecksSecurity += PlayerChecksSecurity;
        EventBroker.PlayerDirectAttacks += PlayerDirectAttacks;
    }

    [Command(requiresAuthority = false)]
    public void CmdStartGame()
    {
        Debug.Log(netId + this.name + " starting game");
        player = new Player();
        RpcStartGame();
    }

    [ClientRpc]
    public void RpcStartGame()
    {
        if (isClientOnly && isLocalPlayer || isServer && !isLocalPlayer)
        {
            SetState(new RaisingPhase(GameManager, this));
        }
        else
        {
            SetState(new EndPhase(GameManager, this));
        }

        if (!hasAuthority) return;

        for (int i = 0; i < 5; i++)
        {
            CmdDrawCardToHand();
            CmdDrawCardToSecurity();
        }
    }

    [Command]
    public void CmdDrawCardToHand()
    {
        GameObject card = player.DrawCard();
        NetworkServer.Spawn(card, connectionToClient);

        player.AddCardToHand(card);
        RpcAddedCardToHand(card);
    }

    [ClientRpc]
    public void RpcAddedCardToHand(GameObject card)
    {
        //Maybe add authority
        EventBroker.CallAddedCardToHand(card, hasAuthority);
    }

    [Command]
    public void CmdDrawCardToSecurity()
    {
        GameObject card = player.DrawCard();
        NetworkServer.Spawn(card, connectionToClient);

        player.AddSecurity(card);
        RpcAddedCardToSecurity(card);
    }

    [ClientRpc]
    public void RpcAddedCardToSecurity(GameObject card)
    {
        //Maybe add authority
        EventBroker.CallAddedCardToSecurity(card, hasAuthority);
    }
    #endregion

    #region Raising Phase
    public void PlayerHatchesEgg()
    {
        StartCoroutine(State.HatchDigitama());
    }

    [Command]
    public void CmdHatchEgg()
    {
        GameObject raisingCard = player.HatchEgg();
        NetworkServer.Spawn(raisingCard, connectionToClient);

        RpcHatchEgg(raisingCard);
    }

    [ClientRpc]
    public void RpcHatchEgg(GameObject raisingCard)
    {
        EventBroker.CallHatchedEgg(raisingCard, hasAuthority);
    }


    public void PlayerMovesRaisingCardToBattleArea(GameObject card)
    {
        StartCoroutine(State.PlayCard(card));
    }

    [Command]
    public void CmdMoveRaisingCardToBattleArea(GameObject card)
    {
        player.AddCardToBattleArea(card);
        RpcMoveRaisingCardToBattleArea(card);
    }

    [ClientRpc]
    public void RpcMoveRaisingCardToBattleArea(GameObject card)
    {
        EventBroker.CallAddedRaisingCardToBattleArea(card, hasAuthority);
    }
    #endregion

    #region Main Phase
    public void PlayerPlaysCardFromHandToBattleArea(GameObject card)
    {
        StartCoroutine(State.PlayCard(card));
    }

    [Command]
    public void CmdPlaysCardFromHandToBattleArea(GameObject card)
    {
        player.RemoveCardFromHand(card);
        player.AddCardToBattleArea(card);
        UseMemory(card);
        //Use Memory
        RpcPlaysCardFromHandToBattleArea(card);

    }

    [ClientRpc]
    public void RpcPlaysCardFromHandToBattleArea(GameObject card)
    {
        EventBroker.CallPlayedCardFromHandToBattleArea(card, hasAuthority);
    }

    public void PlayerAttacksCard(GameObject attacker, GameObject target)
    {
        StartCoroutine(State.Attack(attacker, target));
    }

    [Command]
    public void CmdResolveAttack(GameObject attacker, GameObject target)
    {
        AttackHandler.ResolveAttack(attacker, target);
    }

    [Server]
    public void ResolveAttack(GameObject attacker, GameObject target, Enums.BATTLE_OUTCOMES result)
    {
        if (Enums.BATTLE_OUTCOMES.WIN.Equals(result))
        {
            //Set card to rest mode
        }
        else if (Enums.BATTLE_OUTCOMES.LOSE.Equals(result))
        {
            player.RemoveCardFromBattleArea(attacker);
            player.AddToDiscardPile(attacker);
        }
        else if (Enums.BATTLE_OUTCOMES.TIE.Equals(result))
        {
            player.RemoveCardFromBattleArea(attacker);
            player.AddToDiscardPile(attacker);
        }

        TargetResolveAttack(attacker, target, result);
    }

    [TargetRpc]
    public void TargetResolveAttack(GameObject attacker, GameObject target, Enums.BATTLE_OUTCOMES result)
    {
        EventBroker.CallResolveAttack(attacker, target, result);
    }

    [Command(requiresAuthority = false)]
    public void CmdResolveDefence(GameObject attacker, GameObject target, Enums.BATTLE_OUTCOMES result)
    {
        if (Enums.BATTLE_OUTCOMES.WIN.Equals(result))
        {
            player.RemoveCardFromBattleArea(target);
            player.AddToDiscardPile(target);
        }
        else if (Enums.BATTLE_OUTCOMES.LOSE.Equals(result))
        {
            // Do Nothing
        }
        else if (Enums.BATTLE_OUTCOMES.TIE.Equals(result))
        {
            player.RemoveCardFromBattleArea(target);
            player.AddToDiscardPile(target);
        }

        TargetResolveDefence(attacker, target, result);
    }

    [TargetRpc]
    public void TargetResolveDefence(GameObject attacker, GameObject target, Enums.BATTLE_OUTCOMES result)
    {
        EventBroker.CallResolveDefence(attacker, target, result);
    }

    public void PlayerDirectAttacks(GameObject attacker)
    {
        StartCoroutine(State.DirectAttack(attacker));
    }

    [Command]
    public void CmdDirectAttack(GameObject attacker)
    {
        AttackHandler.ResolveDirectAttack(attacker);
    }

    #endregion

    #region Security
    public void PlayerChecksSecurity(GameObject attacker)
    {
        StartCoroutine(State.CheckSecurity(attacker));
    }

    public GameObject CheckSecurity()
    {
        GameObject securityCard = player.CheckSecurity();
        return securityCard;
    }

    [Command]
    public void CmdPlayerChecksSecurity(GameObject attacker)
    {
        SecurityHandler.ResolveSecurityCheck(attacker);
    }

    [Server]
    public void ResolveSecurityCheck(GameObject attacker, GameObject security, Enums.BATTLE_OUTCOMES result)
    {
        if (Enums.BATTLE_OUTCOMES.WIN.Equals(result))
        {
            //TODO Rest Attacker
        }
        else if (Enums.BATTLE_OUTCOMES.LOSE.Equals(result))
        {
            player.RemoveCardFromBattleArea(attacker);
            player.AddToDiscardPile(attacker);
        }

        TargetResolveSecurityCheck(attacker, security, result);
    }

    [TargetRpc]
    public void TargetResolveSecurityCheck(GameObject attacker, GameObject security, Enums.BATTLE_OUTCOMES result)
    {
        EventBroker.CallResolveSecurityCheck(attacker, security, result);
    }

    [Server]
    public void ResolveSecurityBeingChecked(GameObject attacker, GameObject security, Enums.BATTLE_OUTCOMES result)
    {
        TargetResolveSecurityBeingChecked(attacker, security, result);
    }

    [TargetRpc]
    public void TargetResolveSecurityBeingChecked(GameObject attacker, GameObject security, Enums.BATTLE_OUTCOMES result)
    {
        EventBroker.CallResolveSecurityBeingChecked(attacker, security, result);
    }
    #endregion

    [Server]
    public void EndGame(bool win){
        TargetEndGame(win);
    }

    [TargetRpc]
    public void TargetEndGame(bool win){
        UIEventBroker.CallEndGame(win);
    }

    private void UseMemory(GameObject card)
    {
        int cost = card.GetComponent<BasicCard>().memoryCost;
        GameManager.RpcUseMemory(cost);
    }

}
