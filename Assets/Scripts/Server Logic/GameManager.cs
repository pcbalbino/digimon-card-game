﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class GameManager : NetworkBehaviour
{
    public PlayerManager Player;

    [SyncVar]
    private int numberOfPlayers = 0;
    private int memory = 0;

    public override void OnStartClient()
    {
        base.OnStartClient();
        StartCoroutine(StartClients());
    }

    public IEnumerator StartClients()
    {
        numberOfPlayers++;

        yield return new WaitForSeconds(1f);

        if (numberOfPlayers == 2)
        {
            PlayerManager[] players = GameObject.FindObjectsOfType<PlayerManager>();
            Debug.Log(players.ToString());

            Player = NetworkClient.connection.identity.GetComponent<PlayerManager>();

            players[0].CmdStartGame();
            players[1].CmdStartGame();
        }
    }

    [ClientRpc]
    public void RpcUseMemory(int cost)
    {
        PlayerManager playerManager = NetworkClient.connection.identity.GetComponent<PlayerManager>();

        if (!playerManager.hasAuthority) return;

        int previousMemory = memory;

        if (!"EndPhase".Equals(playerManager.GetState()))
        {
            memory += cost;
            if (memory > 0)
            {
                playerManager.SetState(new EndPhase(this, playerManager));
            }
        }
        else
        {
            memory -= cost;
            if (memory < 0)
            {
                playerManager.SetState(new ActivePhase(this, playerManager));
            }
        }

        EventBroker.CallUsedMemory(memory);
    }

    public void ChangePhase(string phase)
    {
        //string phaseChange = currentPlayer.name + " - " + phase;

        //gameManagerUI.ChangePhase(phaseChange);
    }
}
