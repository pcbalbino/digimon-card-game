﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class SecurityHandler
{
    [Server]
    public static void ResolveSecurityCheck(GameObject attacker)
    {
        PlayerManager attackingPlayer = attacker.GetComponent<NetworkIdentity>()
            .connectionToClient.identity.gameObject.GetComponent<PlayerManager>();

        PlayerManager targetPlayer = null;

        foreach (PlayerManager player in GameObject.FindObjectsOfType<PlayerManager>())
        {
            if(!player.Equals(attackingPlayer))
                targetPlayer = player;
        }

        if(targetPlayer == null)
            return;

        GameObject securityChecked = targetPlayer.CheckSecurity();
        Enums.BATTLE_OUTCOMES result;

        if (attacker.GetComponent<BasicCard>().DP > securityChecked.GetComponent<BasicCard>().DP)
        {
            result =  Enums.BATTLE_OUTCOMES.WIN;
        }
        else
        {
            result =  Enums.BATTLE_OUTCOMES.LOSE;
        }

        attackingPlayer.ResolveSecurityCheck(attacker, securityChecked, result);
        targetPlayer.ResolveSecurityBeingChecked(attacker, securityChecked, result);
    }

}
