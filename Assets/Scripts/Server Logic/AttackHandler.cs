﻿using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;

public class AttackHandler //: NetworkBehaviour
{
    PlayerManager player1;
    PlayerManager player2;

    void Start()
    {
        //player1 = NetworkClient.connection.identity.GetComponent<PlayerManager>();
        //player2 = NetworkServer.connections.
    }

    [Server]
    public static void ResolveAttack(GameObject attacker, GameObject target)
    {
        NetworkIdentity netId = attacker.GetComponent<NetworkIdentity>();

        PlayerManager attackingPlayer = netId
            .connectionToClient.identity.gameObject.GetComponent<PlayerManager>();

        netId = target.GetComponent<NetworkIdentity>();

        PlayerManager targetPlayer = netId
            .connectionToClient.identity.gameObject.GetComponent<PlayerManager>();

        Enums.BATTLE_OUTCOMES result;

        if (attacker.GetComponent<BasicCard>().DP > target.GetComponent<BasicCard>().DP)
        {
            result = Enums.BATTLE_OUTCOMES.WIN;
        }
        else if (attacker.GetComponent<BasicCard>().DP < target.GetComponent<BasicCard>().DP)
        {
            result = Enums.BATTLE_OUTCOMES.LOSE;
        }
        else
        {
            result = Enums.BATTLE_OUTCOMES.TIE;
        }

        attackingPlayer.ResolveAttack(attacker, target, result);
        targetPlayer.CmdResolveDefence(attacker, target, result);
    }

    [Server]
    public static void ResolveDirectAttack(GameObject attacker)
    {
        PlayerManager attackingPlayer = attacker.GetComponent<NetworkIdentity>()
            .connectionToClient.identity.gameObject.GetComponent<PlayerManager>();

        PlayerManager targetPlayer = null;

        foreach (PlayerManager player in GameObject.FindObjectsOfType<PlayerManager>())
        {
            if(!player.Equals(attackingPlayer))
                targetPlayer = player;
        }

        if(targetPlayer == null)
            return;

        attackingPlayer.EndGame(true);
        targetPlayer.EndGame(false);
    }

}
