﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mirror;

public class GameManagerUI : NetworkBehaviour
{
    private GameObject phaseChange;
    [SerializeField]
    private GameObject endGameSign;

    void Start()
    {
        UIEventBroker.EndGame += EndGame;
    }

    public void ChangePhase(string status)
    {
        phaseChange.SetActive(true);
        phaseChange.GetComponentInChildren<Text>().text = status;

        LeanTween.scale(phaseChange, new Vector3(1f, 1f, 1f), 1.5f).setEase(LeanTweenType.easeOutBack)
            .setOnComplete(ResetObject);
        //LeanTween.moveLocalX(phaseChange, 1950, 2f).setOnComplete(ResetObject);
    }

    private void EndGame(bool win)
    {
        endGameSign.SetActive(true);

        if (win)
            endGameSign.GetComponentInChildren<Text>().text = "You win! =D";
        else
            endGameSign.GetComponentInChildren<Text>().text = "You lose! =/";

        endGameSign.transform.localScale = new Vector3(0f,0f,0f);
        LeanTween.scale(endGameSign, new Vector3(1f, 1f, 1f), 1.5f).setEase(LeanTweenType.easeOutBack);
    }

    private void ResetObject()
    {

        LeanTween.scale(endGameSign, Vector3.zero, 1f).setEase(LeanTweenType.easeOutBack);
        phaseChange.SetActive(false);
        //phaseChange.transform.localPosition = new Vector2(-1950, 0);
    }

}
