﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Mirror;

public class EventBroker : NetworkBehaviour
{
    public static event Action LoadGame;
    public static event Action<int> CardDrawn;
    public static event Action<GameObject, bool> AddedCardToHand;
    public static event Action<GameObject, bool> AddedCardToSecurity;
    public static event Action<GameObject, bool> HatchedEgg;
    public static event Action<GameObject, bool> AddedRaisingCardToBattleArea;
    public static event Action<GameObject, bool> PlayedCardFromHandToBattleArea;
    public static event Action<int> UsedMemory;

    public static void CallLoadGame()
    {
        if (LoadGame != null)
            LoadGame();
    }
    public static void CallCardDrawn(int remainingCards)
    {
        if (CardDrawn != null)
            CardDrawn(remainingCards);
    }
    public static void CallAddedCardToHand(GameObject card, bool isPlayer1)
    {
        if (AddedCardToHand != null)
            AddedCardToHand(card, isPlayer1);
    }
    public static void CallAddedCardToSecurity(GameObject card, bool isPlayer1)
    {
        if (AddedCardToSecurity != null)
            AddedCardToSecurity(card, isPlayer1);
    }
    public static void CallHatchedEgg(GameObject card, bool isPlayer1)
    {
        if (AddedCardToSecurity != null)
            HatchedEgg(card, isPlayer1);
    }
    public static void CallAddedRaisingCardToBattleArea(GameObject card, bool isPlayer1)
    {
        if (AddedRaisingCardToBattleArea != null)
            AddedRaisingCardToBattleArea(card, isPlayer1);
    }

    public static void CallPlayedCardFromHandToBattleArea(GameObject card, bool isPlayer1)
    {
        if (PlayedCardFromHandToBattleArea != null)
            PlayedCardFromHandToBattleArea(card, isPlayer1);
    }

    public static void CallUsedMemory(int newMemory)
    {
        if(UsedMemory != null)
            UsedMemory(newMemory);
    }

    #region Player Events
    public static event Action PlayerHatchesEgg;
    public static event Action<GameObject> PlayerMovesRaisingCardToBattleArea;
    public static event Action<GameObject> PlayerPlaysCardFromHandToBattleArea;
    public static event Action<GameObject, GameObject> PlayerAttacksCard;
    public static event Action<GameObject> PlayerChecksSecurity;
    public static event Action<GameObject> PlayerDirectAttacks;

    public static void CallPlayerHatchesEgg()
    {
        if (PlayerHatchesEgg != null)
            PlayerHatchesEgg();
    }

    public static void CallsPlayerMovesRaisingCardToBattleArea(GameObject card)
    {
        if (PlayerMovesRaisingCardToBattleArea != null)
            PlayerMovesRaisingCardToBattleArea(card);
    }

    public static void CallsPlayerPlaysCardFromHandToBattleArea(GameObject card)
    {
        if (PlayerPlaysCardFromHandToBattleArea != null)
            PlayerPlaysCardFromHandToBattleArea(card);
    }

    public static void CallPlayerAttacksCard(GameObject attacker, GameObject target)
    {
        if(PlayerAttacksCard != null)
            PlayerAttacksCard(attacker, target);
    }

    public static void CallPlayerChecksSecurity(GameObject attacker)
    {
        if(PlayerChecksSecurity != null)
            PlayerChecksSecurity(attacker);
    }

    public static void CallPlayerDirectAttacks(GameObject attacker){
        if(PlayerDirectAttacks != null)
            PlayerDirectAttacks(attacker);
    }
    #endregion

    #region CARD EVENTS
    public static event Action<GameObject, GameObject, Enums.BATTLE_OUTCOMES> ResolveAttack;
    public static event Action<GameObject, GameObject, Enums.BATTLE_OUTCOMES> ResolveDefence;
    public static void CallResolveAttack(GameObject attacker, GameObject target, Enums.BATTLE_OUTCOMES result)
    {
        if(ResolveAttack != null)
            ResolveAttack(attacker, target, result);
    }
    public static void CallResolveDefence(GameObject attacker, GameObject target, Enums.BATTLE_OUTCOMES result)
    {
        if(ResolveDefence != null)
            ResolveDefence(attacker, target, result);
    }
    #endregion
    #region Security
    public static event Action<GameObject, GameObject, Enums.BATTLE_OUTCOMES> ResolveSecurityCheck;
    public static event Action<GameObject, GameObject, Enums.BATTLE_OUTCOMES> ResolveSecurityBeingChecked;

    public static void CallResolveSecurityCheck(GameObject attacker, GameObject target, Enums.BATTLE_OUTCOMES result)
    {
        if(ResolveSecurityCheck != null)
            ResolveSecurityCheck(attacker, target, result);

    }

    public static void CallResolveSecurityBeingChecked(GameObject attacker, GameObject target, Enums.BATTLE_OUTCOMES result)
    {
        if(ResolveSecurityBeingChecked != null)
            ResolveSecurityBeingChecked(attacker, target, result);
    }
    #endregion
}
