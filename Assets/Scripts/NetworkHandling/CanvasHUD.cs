﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mirror;
using UnityEngine.SceneManagement;

public class CanvasHUD : MonoBehaviour
{
    public Button buttonHost;
    public Button buttonJoin;

    public void Start()
    {
    }

    public void ButtonHost()
    {
        DeckV3 deck = new DeckV3();
        deck.name = "test";
        deck.cards[0] = "1";
        deck.cards[1] = "2";

        deck.digitamaCards[0] = "3";
        deck.digitamaCards[1] = "4";

        //SaveSystem.SaveDeck(deck);

        //NetworkManager.singleton.StartHost();

        SaveSystem.LoadDeck();
    }

    public void ButtonJoin()
    {
        NetworkManager.singleton.StartClient();
    }

    public void QuitMatch()
    {
        SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene());
        NetworkManager.singleton.StopHost();
    }

    public void OnDiscoery()
    {

    }
}
