﻿using System.Collections.Generic;
using UnityEngine;
using Mirror;
using Mirror.Discovery;
using UnityEngine.UI;

public class CustomNetworkDiscoveryHUD : MonoBehaviour
{
    readonly Dictionary<long, ServerResponse> discoveredServers = new Dictionary<long, ServerResponse>();
    public ScrollRect serversScrollView;
    public GameObject buttonPrefab;

    public NetworkDiscovery networkDiscovery;
    public Button discoverServersButton;
    public Button buttonHost;


#if UNITY_EDITOR
    void OnValidate()
    {
        if (networkDiscovery == null)
        {
            networkDiscovery = GetComponent<NetworkDiscovery>();
            UnityEditor.Events.UnityEventTools.AddPersistentListener(networkDiscovery.OnServerFound, OnDiscoveredServer);
            UnityEditor.Undo.RecordObjects(new Object[] { this, networkDiscovery }, "Set NetworkDiscovery");
        }
    }
#endif

    public void Start()
    {
        buttonHost.onClick.AddListener(ButtonHost);
        discoverServersButton.onClick.AddListener(DiscoverServers);

    }
    public void ButtonHost()
    {
        NetworkManager.singleton.StartHost();
        networkDiscovery.AdvertiseServer();
    }
    public void DiscoverServers()
    {
        //discoveredServers.Clear();
        networkDiscovery.StartDiscovery();
        // Clean MatchesUI
    }

    void Connect(ServerResponse info)
    {
        networkDiscovery.StopDiscovery();
        NetworkManager.singleton.StartClient(info.uri);
    }

    public void OnDiscoveredServer(ServerResponse info)
    {
        if (discoveredServers.ContainsKey(info.serverId))
            return;

        discoveredServers.Add(info.serverId, info);

        // Note that you can check the versioning to decide if you can connect to the server or not using this method
        GameObject serverButtonObject = GameObject.Instantiate(buttonPrefab, serversScrollView.content, false);
        serverButtonObject.SetActive(true);
        Button serverButton = serverButtonObject.GetComponent<Button>();
        serverButton.GetComponentInChildren<Text>().text = info.uri.ToString();
        serverButton.onClick.AddListener(delegate { Connect(info); });
    }



}